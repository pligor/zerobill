package models

import net.sqlcipher.database.SQLiteDatabase

object TableCreator {

  def apply(db: SQLiteDatabase): Unit = {

  }

  val createStatements = List(
    Operator.createStatement,
    PhoneOperator.createStatement,
    PhoneCall.createStatement
  )

  val dropTables = List(
    PhoneCall.tableName,
    PhoneOperator.tableName,
    Operator.tableName
  )

  def createAll(db: SQLiteDatabase): Unit = createStatements foreach {
    createStatement =>
      db.execSQL(createStatement.stripMargin)
  }

  def dropAll(db: SQLiteDatabase): Unit = dropTables foreach {
    tableName =>
      db.execSQL(s"DROP TABLE $tableName")
  }

}
