package models

import net.sqlcipher.database.SQLiteDatabase
import android.content.{ContentValues, Context}

case class Operator(id:Option[Int], name:String)

object Operator {

  val tableName = "carrier"

  val ID = "id"
  val NAME = "name"

  val createStatement =
    s"""CREATE TABLE $tableName (
      |  $ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
      |  $NAME TEXT NOT NULL
      |)"""

  def createTable(db: SQLiteDatabase)(implicit ctx: Context) = {
    DatabaseHandler.getInstance.execSQLdb(db, createStatement.stripMargin)
  }

  def insert(operator:Operator)(implicit ctx: Context) = {

    val cv = new ContentValues()

    cv.put(NAME, operator.name)

    DatabaseHandler.getInstance.insert(tableName, cv)

  }
}
