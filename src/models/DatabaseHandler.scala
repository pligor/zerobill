package models

import android.content.{ContentValues, Context}
import com.pligor.myandroid.sqlite.{MySQLiteOpenHelper, MySQLiteOpenHelperSecure}
import android.database.Cursor
import java.io._

import net.sqlcipher.database.SQLiteDatabase
import android.os.Environment

object DatabaseHandler {

  private val SUBPATH_DIRECTORY = "fcarrier"
  val FULLPATH_DIRECTORY = sdSubpath2fullpath(SUBPATH_DIRECTORY)

  def sdSubpath2fullpath(subpath: String) = {
    Environment.getExternalStorageDirectory + File.separator + subpath
  }

  private val DATABASE_NAME = "fcarrier"

  private val DATABASE_VERSION = 2

  private val DB_PASS = """dci5E7PopX3NoMgX7MdjWn2UA9wYXSSKdnJM8X7u5yuHc4YOKFr6BY53TDWgrGKRWgUUWAXvnxUvRiXb"""

  def safeCloseCursor(cursorOption: Option[Cursor]): Unit = {
    cursorOption.map(c => if (!c.isClosed) {
      c.close()
    })
  }

  def safeCloseDb(dbOption: Option[SQLiteDatabase]): Unit = {
    dbOption.map(d => if (d.isOpen) {
      d.close()
    })
  }

  def getInstance(implicit context: Context) = {
    //if you do NOT want a secure encrypted database also comment this line
    MySQLiteOpenHelper.loadSqlcipherLibs

    new DatabaseHandler()
  }
}

/**
 * We create this object for the specific database, NOT for each table
 */
class DatabaseHandler(implicit context: Context) extends MySQLiteOpenHelperSecure(
  DatabaseHandler.FULLPATH_DIRECTORY,
  DatabaseHandler.DATABASE_NAME,
  DatabaseHandler.DATABASE_VERSION,
  dbPass = DatabaseHandler.DB_PASS,
  creator = TableCreator.apply
) {
  def populate(implicit context: Context): Unit = {
    throw new Exception("IMPLEMENT")
    reset()
    //TablePopulator(insert);
  }

  protected def populator(insert: (String, ContentValues) => Long)
                         (implicit context: Context): Unit = {
    //TestingTablePopulator(insert);
  }

  /**
   * http://blog.adamsbros.org/2012/02/28/upgrade-android-sqlite-database/
   */
  protected def onUpgrade(db: SQLiteDatabase,
                          oldVersion: Int,
                          newVersion: Int): Unit = {
    //drop older tables
    //create some tables again, etc.
    //do nothing for the time being

    var upgradeTo = oldVersion + 1

    while (upgradeTo <= newVersion) {
      upgradeTo match {
        case 2 =>
          //PhoneCall.createTable(db)
      }

      upgradeTo += 1
    }
  }
}
