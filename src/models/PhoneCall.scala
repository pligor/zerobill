package models

import net.sqlcipher.database.SQLiteDatabase
import android.content.{ContentValues, Context}
import components.CallType
import android.database.Cursor
import org.joda.time.DateTime

case class PhoneCall(
  id:Option[Int] = None,
  duration:Long,
  called:DateTime,
  callType:CallType,
  phoneOperatorId:Option[Int])

object PhoneCall {

  val tableName = "phonecall"

  val ID = "id"
  val CALL_TYPE = "calltype"
  val DURATION = "duration"
  val CALLED = "called"
  val PHONE_OPERATOR_ID = "phone_operator_id"

  val createStatement =
    s"""CREATE TABLE $tableName (
      |  $ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
      |  $CALL_TYPE INTEGER NOT NULL,
      |  $DURATION INTEGER NOT NULL,
      |  $CALLED INTEGER NOT NULL,
         $PHONE_OPERATOR_ID INTEGER,
      |  FOREIGN KEY($PHONE_OPERATOR_ID) REFERENCES ${PhoneOperator.tableName}(${PhoneOperator.ID})
      |    ON DELETE CASCADE ON UPDATE RESTRICT
      |)"""

  def createTable(db: SQLiteDatabase)(implicit ctx: Context) = {
    DatabaseHandler.getInstance.execSQLdb(db, createStatement.stripMargin)
  }

  def getAll(implicit context: Context) = {
    DatabaseHandler.getInstance.getAll(tableName)(createFromCursor)
  }

  def insert(phoneCall:PhoneCall)(implicit ctx: Context) = {

        val cv = new ContentValues()

        cv.put(CALL_TYPE, new java.lang.Integer(phoneCall.callType.callTypeId))

        cv.put(DURATION, new java.lang.Long(phoneCall.duration))

        cv.put(CALLED, new java.lang.Long(phoneCall.called.getMillis))

        phoneCall.phoneOperatorId.foreach{ id =>
          cv.put(PHONE_OPERATOR_ID, new java.lang.Integer(id))
        }

        DatabaseHandler.getInstance.insert(PhoneCall.tableName, cv)

  }

  def createFromCursor(cursor: Cursor) = {

    val id = cursor.getInt(cursor.getColumnIndexOrThrow(ID))
    val callType = cursor.getInt(cursor.getColumnIndexOrThrow(CALL_TYPE))
    val phoneOperatorId = cursor.getInt(cursor.getColumnIndexOrThrow(PHONE_OPERATOR_ID))
    val duration = cursor.getLong(cursor.getColumnIndexOrThrow(DURATION))
    val called = cursor.getLong(cursor.getColumnIndexOrThrow(CALLED))

    PhoneCall(
      id = Some(id),
      callType = CallType.resolve(callType),
      phoneOperatorId = Some(phoneOperatorId),
      duration = duration,
      called = new DateTime(called)
    )
  }
}