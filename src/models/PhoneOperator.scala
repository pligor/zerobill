package models

import net.sqlcipher.database.SQLiteDatabase
import android.content.{ContentValues, Context}
import components.CallType

case class PhoneOperator(
  id:Option[Int] = None,
  operatorId:Int,
  phoneNumber:String,
  phoneE164:String,
  countryCode:String,
  wired:Boolean)

object PhoneOperator {

  val tableName = "phoneoperator"

  val ID = "id"
  val PHONE_NUMBER = "phone_number"
  val PHONE_E164 = "phone_E164"
  val WIRED = "wired"
  val COUNTRY_CODE = "country_code"
  val OPERATOR_ID = "operator_id"

  val createStatement =
    s"""CREATE TABLE $tableName (
         $ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
         $PHONE_NUMBER TEXT NOT NULL,
         $PHONE_E164 TEXT NOT NULL,
         $WIRED INTEGER NOT NULL,
         $COUNTRY_CODE TEXT NOT NULL,
         $OPERATOR_ID INTEGER NOT NULL,
         FOREIGN KEY($OPERATOR_ID) REFERENCES ${Operator.tableName}(${Operator.ID})
           ON DELETE CASCADE ON UPDATE RESTRICT
       )"""

  def createTable(db: SQLiteDatabase)(implicit ctx: Context) = {
    DatabaseHandler.getInstance.execSQLdb(db, createStatement.stripMargin)
  }

  def insert(phoneOperator:PhoneOperator)(implicit ctx: Context) = {

    val cv = new ContentValues()

    cv.put(COUNTRY_CODE, phoneOperator.countryCode)

    cv.put(PHONE_NUMBER, phoneOperator.phoneNumber)

    cv.put(PHONE_E164, phoneOperator.phoneE164)

    cv.put(WIRED, new Integer(if(phoneOperator.wired) 1 else 0))

    DatabaseHandler.getInstance.insert(tableName, cv)

  }
}
