package activities

import org.scaloid.common._
import scala.concurrent.future
import scala.concurrent.ExecutionContext.Implicits.global
import components._
import models._
import scala.Some
import org.joda.time.DateTime
import adapters.ContactAdapter
import com.pligor.fcarrier.{R, TR, TypedViewHolder}
import android.text.{Editable, TextWatcher}
import android.content.Context
import android.os.Bundle
import android.widget.ListView


/**
 * Created by rudolfmarkulin on 29/04/14.
 */
class MainActivity extends SActivity with TypedViewHolder with LoginRequestPreferences {

  private lazy val filterEditText = findView(TR.filterEditText)
  private lazy val contactListView:ListView = findView(TR.contactListView)
  var contactAdapter:ContactAdapter = null

  override def onCreate(savedInstanceState: Bundle): Unit = {
    super.onCreate(savedInstanceState)

    setContentView(R.layout.activity_main)
    contactAdapter = new ContactAdapter(contactListView, getBaseContext, Contact.getPhoneNumbersAlongWithNames.toArray)

    val fut = future {
      warn("EEE")
      getLoginRequest match {
        case Some(loginRequest) => {
          import com.google.i18n.phonenumbers.Phonenumber.PhoneNumber
          import com.pligor.phonenumber.PhoneNumberHelper
          warn("MM" + PhoneNumberHelper.parsePhoneString("+306956775925").map(pn => OperatorService.getOperatorByPhone(pn)(loginRequest)))
        }//PaymentService.save (Payment ("111", "222") )(loginRequest).toString
        case None =>
      }
    }

    filterEditText.addTextChangedListener(new TextWatcher() {
      override def onTextChanged(cs:CharSequence, arg1:Int, arg2:Int, arg3:Int) {
        MainActivity.this.contactAdapter.getFilter().filter(cs);
      }

      override def beforeTextChanged( arg0:CharSequence, arg1:Int, arg2:Int, arg3:Int) {}
      override def afterTextChanged(arg0:Editable) {}
    });

    contactListView.setAdapter(contactAdapter)

    val operators = future {

        DatabaseHandler.getInstance.workWithWritableDatabase{db =>
          TableCreator.dropAll(db)
          TableCreator.createAll(db)
        }

        val tt:Long = PhoneCall.insert(PhoneCall(duration = 10000, called = DateTime.now, callType = CallType.OUTGOING, phoneOperatorId = None))
        PhoneCall.getAll.foreach(p => warn("Main " + p.toString))
        warn("Main PC insert" + tt)
    }

    operators.map(a => warn(a.toString)).onFailure{
      case a:Throwable => runOnUiThread(alert("tt", a.toString))
    }

    //val pf: PartialFunction[Throwable, Unit] = (a:Throwable) => runOnUiThread(alert(a.toString, "tt"))
    fut.map(a => warn(a.toString)).onFailure{
      case a:Throwable => runOnUiThread(alert(a.toString, "tt"))
    }


  }
}
