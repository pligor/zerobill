package activities

import org.scaloid.common._
import android.support.v4.app.Fragment
import android.view.{View, ViewGroup, LayoutInflater}
import android.os.Bundle
import com.pligor.fcarrier.{R, TR, TypedViewHolder}
import android.widget.{ImageView, TextView}

class LoginWizardFragment(step:Int) extends Fragment {

  override def onCreateView(inflater:LayoutInflater,container:ViewGroup, savedInstanceState:Bundle):View = {

    val rootView = inflater.inflate(R.layout.fragment_login_wizard, container, false).asInstanceOf[View]
    val description = rootView.findViewById(R.id.loginWizardDescription).asInstanceOf[TextView]
    val image = rootView.findViewById(R.id.loginWizardImageView).asInstanceOf[ImageView]

    implicit val context = container.getContext

    description.setText("Step " + step)
    step match {
      case 0 => image.setImageDrawable(R.drawable.w1)
      case 1 => image.setImageDrawable(R.drawable.w2)
      case 2 => image.setImageDrawable(R.drawable.w3)
      case 3 => image.setImageDrawable(R.drawable.w4)
      case 4 => image.setImageDrawable(R.drawable.w5)
    }


    rootView
  }
}
