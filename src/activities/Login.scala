package activities

import org.brickred.socialauth.android.{SocialAuthListener, SocialAuthError, DialogListener, SocialAuthAdapter}
import org.brickred.socialauth.Profile
import org.scaloid.common._
import android.os.Bundle
import com.pligor.fcarrier.{R, TR, TypedViewHolder}
import android.widget.{ImageButton, Button}
import components.AndroidHelper._
import android.view.View
import android.content.{Context, Intent}
import components.{Response, LoginService, LoginRequestPreferences, UserInfoPreferences}
import com.pligor.zerobill_request_response.LoginRequest
import scala.collection.JavaConversions._
import android.support.v4.app._

class Login extends FragmentActivity with TagUtil with SContext with TypedViewHolder with LoginRequestPreferences {

  private lazy val socialAuthAdapter = new SocialAuthAdapter(ResponseListener)

  lazy val progressInfo = findView(TR.loginInfoProgressBar)

  override def onCreate(savedInstanceState: Bundle) = {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_login)

    findView(TR.loginViewpager).setAdapter(new LoginWizardPagerAdapter(getSupportFragmentManager))

    val connected = isAndroidConnectedOnline
    findView(TR.loginButtons).visibility(viewVisible(connected))
    findView(TR.loginNextButton).visibility(viewVisible(!connected)).onClick(proceed _)

    findView(TR.loginFacebookButton).onClick(startAuthentication(SocialAuthAdapter.Provider.FACEBOOK))
    findView(TR.loginGoogleButton).onClick(startAuthentication(SocialAuthAdapter.Provider.GOOGLE))
    findView(TR.loginTwitterButton).onClick(startAuthentication(SocialAuthAdapter.Provider.TWITTER))
    findView(TR.loginLinkedinButton).onClick(startAuthentication(SocialAuthAdapter.Provider.LINKEDIN))

    progressInfo.visibility(viewVisible(false))
  }

  private def startAuthentication(provider:SocialAuthAdapter.Provider) = {
    progressInfo.visibility(viewVisible(true))
    socialAuthAdapter.authorize(ctx, provider)
  }

  private def proceed(view:View) = {
    retrieveInfoAutomaticaly
  }

  private class LoginWizardPagerAdapter(val fm: FragmentManager) extends FragmentPagerAdapter(fm) {

    val NUM_PAGES = 5

    override def getItem(position:Int):Fragment = {
      new LoginWizardFragment(position)
    }

    override def getCount():Int = {
      NUM_PAGES
    }
  }

  private object ResponseListener extends DialogListener {
    def onBack(): Unit = {
      warn("back was pressed for social auth")
    }

    def onCancel(): Unit = {
      warn("we cancelled social auth")
    }

    def onError(socialAuthError: SocialAuthError): Unit = {
      error(s"social auth error occured: ${socialAuthError.getMessage}")
    }

    def onComplete(bundle: Bundle): Unit = {

      socialAuthAdapter.getUserProfileAsync(new SocialAuthListener[Profile] {
        def onError(socialAuthError: SocialAuthError): Unit = {
          warn("on error you must do something")
        }

        def onExecute(provider: String, profile: Profile): Unit = {

          val grantAccess = socialAuthAdapter.getCurrentProvider.getAccessGrant

          val loginRequest = LoginRequest(
            provider = grantAccess.getProviderId,
            unique_id = profile.getValidatedId,
            consumer_secret = Option(grantAccess.getSecret),
            access_token = grantAccess.getKey,
            attributes = LoginRequest.mapToAttributes(grantAccess.getAttributes.toMap)
            )

          LoginService.login(loginRequest) match {
            case Response(200, _) =>  {
              setLoginRequest(loginRequest)
              retrieveInfoAutomaticaly
            }
            case Response(406, _) =>  {
              setLoginRequest(loginRequest)
              retrieveInfoAutomaticaly
            }
            case Response(code, _) => {
              alert("Login error", s"With code $code")
            }//todo
          }
        }
      })
    }
  }

  def retrieveInfoAutomaticaly() = {
    progressInfo.visibility(viewVisible(true))
    getUserInfo match {
      case Some(number) =>
      case None =>
    }
    startActivity(SIntent[MainActivity])
    finish()
  }

}
