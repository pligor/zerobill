package com.pligor.myandroid.sqlite

import android.content.Context
import android.content.ContextWrapper
import java.io.File

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */

/**
 * http://stackoverflow.com/a/9168969/720484
 * Useful to save the database in SD card
 * @param absoluteDirectoryPath the directory under which an sqlite database will be created
 */
class DatabaseContext(val absoluteDirectoryPath: String)(implicit val context: Context) extends ContextWrapper(context) {
  //dropping db extension because of some stuff that could not be managed
  //private val dbExtension = Some(".db")

  override def getDatabasePath(name: String): File = {
    val dbPath: String = absoluteDirectoryPath + File.separator + {
      /*if (dbExtension.isDefined && !name.endsWith(dbExtension.get)) name + dbExtension.get
      else*/
      name
    }

    val result = new File(dbPath)

    val parentFile = result.getParentFile
    if (parentFile.exists) {
      //nop
    } else {
      parentFile.mkdirs
    }

    result
  }

  /*override def openOrCreateDatabase(name: String, mode: Int, factory: CursorFactory) = {
    //if something wrong with the database consider setting the factory argument to null in the line below
    SQLiteDatabase.openOrCreateDatabase(getDatabasePath(name), password, factory);
  }*/
  override def deleteDatabase(name: String): Boolean = {
    //super.deleteDatabase(name)
    getDatabasePath(name).delete()
  }
}

