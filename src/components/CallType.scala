package components

object CallType {
  import android.provider.CallLog.Calls._

  case object INCOMING extends CallType {
    val callTypeId = INCOMING_TYPE
  }

  case object OUTGOING extends CallType{
    val callTypeId = OUTGOING_TYPE
  }

  case object MISSED extends CallType{
    val callTypeId = MISSED_TYPE
  }

  //apply could not be used
  def resolve: PartialFunction[Int, CallType] = {
    //note that because the first letter is capital this is not treated as a variable but as a value
    case INCOMING_TYPE => INCOMING
    case OUTGOING_TYPE => OUTGOING
    case MISSED_TYPE => MISSED
    case invalidCallTypeId => throw new IllegalArgumentException(s"call type id is invalid: $invalidCallTypeId")
  }
}

trait CallType {
  def callTypeId: Int
}