package components

import android.content.{ContentResolver, Context}
import android.provider.ContactsContract
import scala.collection.mutable
import android.database.Cursor
import scala.language.postfixOps
import com.google.i18n.phonenumbers.Phonenumber.PhoneNumber

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 *
 * TUTORIAL: http://www.androidhub4you.com/2013/06/get-phone-contacts-details-in-android_6.html
 * ID: http://android-contact-id-vs-raw-contact-id.blogspot.in/
 */
case class Contact(phoneBookId:String, displayName:String, phones:List[String])

object Contact {

  //all these because columns inside Contacts Contract are inaccessible (protected)
  private object COLUMNS {
    val DISPLAY_NAME = "display_name"
    val HAS_PHONE_NUMBER = "has_phone_number"
    val CONTACT_ID = "contact_id"
    val _ID = "_id"
  }

  private def workWithCursor[T](cursor: Cursor)(op: Cursor => T): T = {
    try {
      op(cursor)
    } finally {
      cursor.close()
    }
  }

  private def getPhoneNumbersAlongWithNamesByPhonebookId(phoneBookId: String, contentResolver: ContentResolver): Contact = {

    val phoneColumn = ContactsContract.CommonDataKinds.Phone.NUMBER /*NORMALIZED_NUMBER*/
    val nameColumn = COLUMNS.DISPLAY_NAME

    val phoneCursor = contentResolver.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, {
      val projection = Array[String](phoneColumn, nameColumn)
      projection
    },
    COLUMNS.CONTACT_ID + " = ?",
    Array[String](phoneBookId), {
      val sortOrder = null //if you return many rows sort them somehow
      sortOrder
    }
    )

    val phones = mutable.Buffer.empty[String]
    var name = ""

    workWithCursor(phoneCursor) {
      cur =>
        while (cur.moveToNext()) {
          name = cur.getString(cur.getColumnIndexOrThrow(nameColumn))
          phones += cur.getString(cur.getColumnIndexOrThrow(phoneColumn))
        }
    }

    Contact(displayName = name, phoneBookId = phoneBookId, phones = phones.toList)
  }



  def getPhoneNumbersAlongWithNames(implicit context: Context): Seq[Contact] = {
    workWithAllPhoneNumbersFromPhoneBook[Contact](getPhoneNumbersAlongWithNamesByPhonebookId)
  }


  private def workWithAllPhoneNumbersFromPhoneBook[T](workWithPhoneBookId: (String, ContentResolver) => T)
                                                     (implicit context: Context): Seq[T] = {
    implicit val contentResolver = context.getContentResolver

    val cursor = contentResolver.query(ContactsContract.Contacts.CONTENT_URI, {
      val projection = Array[String](COLUMNS._ID, COLUMNS.HAS_PHONE_NUMBER)
      projection
    }, {
      val selection = null
      selection
    }, {
      val selectionArgs = null
      selectionArgs
    }, {
      val sortOrder = null //if you return many rows sort them somehow
      sortOrder
    })

    val buffer = mutable.Buffer.empty[T]

    workWithCursor(cursor) {
      cur =>
        if (cur.getCount > 0) {
          while (cur.moveToNext()) {

            val phoneBookId = cur.getString(cur.getColumnIndexOrThrow(COLUMNS._ID))
            val hasPhoneNumber = cur.getInt(cur.getColumnIndexOrThrow(COLUMNS.HAS_PHONE_NUMBER)) > 0

            if (hasPhoneNumber) {
              buffer += workWithPhoneBookId(phoneBookId, contentResolver)
            } else {
              //no phone number, do not deal with this contact
            }
          }
        } else {
          //EMPTY, no contacts
        }
    }

    buffer.toSeq
  }


  /*
  ALL COLUMNS OF API 17:
  ContactsContract.BaseSyncColumns
    ContactsContract.ContactNameColumns
    ContactsContract.ContactOptionsColumns
    ContactsContract.ContactsColumns
    ContactsContract.ContactStatusColumns
    ContactsContract.DataColumns//.RAW_CONTACT_ID
    ContactsContract.DataColumnsWithJoins
    ContactsContract.GroupsColumns
    ContactsContract.PhoneLookupColumns
    ContactsContract.PresenceColumns
    ContactsContract.RawContactsColumns//.CONTACT ID
    ContactsContract.SettingsColumns
    ContactsContract.StatusColumns
    ContactsContract.StreamItemPhotosColumns
    ContactsContract.StreamItemsColumns//.RAW_CONTACT_ID .CONTACT_ID
    ContactsContract.SyncColumns
   */

}
