package components

import play.api.libs.json.{Format, Json}
import android.preference.PreferenceManager
import android.content.Context
import org.scaloid.common.{SContext, TagUtil}
import com.pligor.zerobill_request_response.LoginRequest

case class UserInfo(
  operatorId:Option[Long],
  phoneNumber:String,
  country:Option[String])

object SharedPreferences {
  def getValue[T](preferenceKey:String)(implicit context:Context, format:Format[T]):Option[T] = {
    val defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
    val info = defaultSharedPreferences.getString(preferenceKey, "{}")
    Json.parse(info).asOpt[T]
  }
  def setValue[T](preferenceKey:String, value:T)(implicit context:Context, format:Format[T]):Boolean = {
    val defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
    val editor = defaultSharedPreferences.edit
    val json = Json.toJson(value)
    editor.putString(preferenceKey, Json.stringify(json))
    editor.commit
  }
}

trait UserInfoPreferences {

  val preferenceKey = "userinfo"

  implicit val formatUserInfo = Json.format[UserInfo]

  def getUserInfo2(implicit context:Context):Option[UserInfo] = {
    SharedPreferences.getValue[UserInfo](preferenceKey)
  }
  def setUserInfo2(userInfo:UserInfo)(implicit context:Context) = {
    SharedPreferences.setValue(preferenceKey, userInfo)
  }
}

trait LoginRequestPreferences {

  val preferenceKey = "loginrequest"

  implicit val format = Json.format[LoginRequest]

  def getLoginRequest(implicit context:Context):Option[LoginRequest] = {
    SharedPreferences.getValue[LoginRequest](preferenceKey)
  }

  def setLoginRequest(loginRequest:LoginRequest)(implicit context:Context):Boolean = {
    SharedPreferences.setValue(preferenceKey, loginRequest)
  }

}