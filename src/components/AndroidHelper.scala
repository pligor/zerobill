package components

import org.scaloid.common._
import android.content.{Context}
import android.net.ConnectivityManager
import android.net.{Uri, NetworkInfo, ConnectivityManager}
import android.view.View


object AndroidHelper {

  def isAndroidConnectedOnline(implicit context: Context): Boolean = {
      Option(connectivityManager.getActiveNetworkInfo).map(_.getState == NetworkInfo.State.CONNECTED).isDefined
  }

  def getUserInfo(implicit context:Context): Option[String] = {
    val number = telephonyManager.getLine1Number
    if(number == null || number.equalsIgnoreCase("")) None
    else Some(number)
  }

  def viewVisible(value:Boolean) = if(value) View.VISIBLE else View.GONE

}
