package components

import com.google.i18n.phonenumbers.Phonenumber.PhoneNumber
import scalaj.http.{HttpOptions, HttpException, Http}
import java.net.SocketTimeoutException
import play.api.libs.json._
import com.pligor.zerobill_request_response.{RetrieveOperatorResponse, RetrieveOperatorRequest, LoginRequest}
import scala.concurrent.duration._
import android.util.Log

object ContentType extends Enumeration {
  val PNG = Value("image/png")

  val JPEG = Value("image/jpeg")

  val APPLICATION_JSON = Value("application/json")

  val TEXT_PLAIN = Value("text/plain")

  def MULTIPART_FORM_DATA(boundary: String) = "multipart/form-data;boundary=" + boundary
}

object HttpHeaderName extends Enumeration {
  val CONTENT_LANGUAGE = Value("Content-Language")

  val CONTENT_TYPE = Value("Content-Type")

  val CONTENT_LENGTH = Value("Content-Length")

  val MY_COUNTRY = Value("My-Country")

  val X_FORWARDED_FOR = Value("X-Forwarded-For") //apache real ip

  val X_REAL_IP = Value("X-Real-IP") //nginx real ip
}

case class Payment(paymentCode:String, userId:String)

case class Response[T](code:Int, data:Option[T])

case class Timeouts(conn:Duration, read:Duration)

object HttpTyped {

  val SECRET_KEY = "vzG3Vp6Px8452557n43Wp7qh5125LSSf"
  val SERVER_HOSTNAME = "http://fcarrier.dnsdynamic.com"

  def postJsonData[D, P](url:String, params:P)(implicit format:Format[D], format2:Format[P], login:LoginRequest, timeouts:Timeouts):Response[D] = {
    try {
      val paramsJson = Json.toJson(params).as[JsObject]
      val loginJson = Json.toJson(login).as[JsObject]
      val json =  paramsJson ++ loginJson + ("zerobill_secret_key" -> JsString(SECRET_KEY))

      Log.e("HTTP", Json.stringify(json))

      val (code, _ , result) = Http.
        postData(url, Json.stringify(json)).
        header(HttpHeaderName.CONTENT_TYPE.toString, ContentType.APPLICATION_JSON.toString).
        option(HttpOptions.connTimeout(timeouts.conn.toMillis.toInt)).
        option(HttpOptions.readTimeout(timeouts.read.toMillis.toInt)).
        asHeadersAndParse(Http.readString)

      Response(code, Some(Json.parse(result).as[D]))
    } catch {
      case e:SocketTimeoutException => {
        Log.e("HTTPTyped", Log.getStackTraceString(e))
        Response(0, None)//todo
      } //todo
      case e:HttpException => Response(e.code, None)
      case e:Exception => {
        Log.e("HTTPTyped", Log.getStackTraceString(e))
        Response(0, None)//todo
      }
    }
  }
}

object PaymentService {

  implicit val timeouts = Timeouts(1.second,1.second)
  implicit val format = Json.format[Payment]

  def save(payment:Payment)(implicit login:LoginRequest) = {
    HttpTyped.postJsonData("http://payment/fortumo", payment)
  }
}

object LoginService {

  implicit val timeouts = Timeouts(2.second,2.second)

  def login(implicit login:LoginRequest) = {
    val loginUrl = HttpTyped.SERVER_HOSTNAME + "/bouncer/login"
    HttpTyped.postJsonData[String, LoginRequest](loginUrl, login)
  }
}

object OperatorService {

  implicit val timeouts = Timeouts(2.second,2.second)

  def getOperatorByPhone(phoneNumber:PhoneNumber)(implicit login:LoginRequest) = {
    val getOperatorUrl = HttpTyped.SERVER_HOSTNAME + "/operator/get"
    HttpTyped.postJsonData[RetrieveOperatorResponse, RetrieveOperatorRequest](getOperatorUrl, RetrieveOperatorRequest(phoneNumber))
  }
}
