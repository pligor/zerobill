package adapters

import android.widget._
import android.view.{LayoutInflater, ViewGroup, View}
import components.Contact
import com.pligor.fcarrier.R
import android.content.Context
import scala.concurrent.ExecutionContext
import android.widget.Filter.FilterResults
import com.nhaarman.listviewanimations.itemmanipulation.ExpandableListItemAdapter

class ContactAdapter(listView:ListView, context:Context, allItems:Array[Contact])(implicit executionContext: ExecutionContext) extends ExpandableListItemAdapter[Contact](context) with Filterable {

  private var items = Array.empty[Contact]

  private val layoutInflater = LayoutInflater.from(context)

  {
    items = allItems
    setAbsListView(listView)
  }


  override def getTitleView(position:Int, convertView:View, parent:ViewGroup) = {
    val contact = getItem(position)

    val view:View = Option(convertView) match {
      case Some(cView) => cView
      case None => layoutInflater.inflate(R.layout.list_item_contact_title, null)
    }

    val name = view.findViewById(R.id.contactTitleNameTextView).asInstanceOf[TextView]
    name.setText(contact.displayName)
    view
  }

  override def getContentView(position: Int, convertView: View, parent: ViewGroup): View = {
    val view = if (Option(convertView).isDefined) {
      convertView
    } else {

      val tempView = layoutInflater.inflate(R.layout.list_item_contact, null)

      tempView.setTag(new ViewHolder(
        contactNameTextView = tempView.findViewById(R.id.contactNameTextView).asInstanceOf[TextView],
        phoneNumberTextView = tempView.findViewById(R.id.phoneNumberTextView).asInstanceOf[TextView],
        carrierNameTextView = tempView.findViewById(R.id.carrierNameTextView).asInstanceOf[TextView],
        carrierLogoImageView = tempView.findViewById(R.id.carrierLogoImageView).asInstanceOf[ImageView]
      ))

      tempView
    }

    val contact = getItem(position)

    val holder = view.getTag.asInstanceOf[ViewHolder]

    holder.contactNameTextView.setText(contact.displayName)

    holder.phoneNumberTextView.setText(contact.phones.headOption.toString)

    //val carrierOption = cc.carrierOption

    //holder.carrierNameTextView.setVisibility(if (carrierOption.isDefined) View.VISIBLE else View.INVISIBLE)

    view
  }

  override def getCount: Int = items.size

  override def getItem(position: Int): Contact = items(position)

  override def getItemId(position: Int): Long = position

  private class ViewHolder(
                            val contactNameTextView: TextView,
                            val phoneNumberTextView: TextView,
                            val carrierNameTextView: TextView,
                            val carrierLogoImageView: ImageView
                            )

  override def getFilter(): Filter = new Filter() {

    override protected def publishResults(constraint: CharSequence, results: FilterResults) {

      items = results.values.asInstanceOf[Array[Contact]]
      notifyDataSetChanged()
    }

    override protected def performFiltering(constraint: CharSequence): FilterResults = {

      val filtered = allItems.filter(_.displayName.contains(constraint.toString))

      val results = new FilterResults()
      results.count = filtered.size
      results.values = filtered

      results
    }
  }

}
