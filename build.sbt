import android.Keys._

android.Plugin.androidBuild  

name := """zerobill"""
 
scalaVersion := "2.10.2"
 
proguardOptions in Android ++= Seq("-dontobfuscate", "-dontoptimize")

libraryDependencies ++= Seq(
  "org.slf4j" % "slf4j-nop" % "1.6.4",
  "org.scaloid" %% "scaloid" % "3.3-8",
  "org.scalaj" %% "scalaj-http" % "0.3.15",
  "com.typesafe.play" %% "play-json" % "2.2.0",
  "com.android.support" % "support-v4" % "19.0.0",
  "com.googlecode.libphonenumber" % "libphonenumber" % "5.9",
  "com.nhaarman.listviewanimations" % "library" % "2.6.0"
  //"com.typesafe.play" %% "play-ws"  % "2.3-M1"
)

scalacOptions in Compile += "-feature"
 
run <<= run in Android
 
install <<= install in Android

proguardOptions in Android ++= Seq(
  "-keep class org.brickred.**",
  "-dontwarn org.brickred.**",
  "-dontwarn javax.naming.InitialContext",
  "-dontnote org.slf4j.**",
  "-keep class scala.collection.Seq.**",
  "-keep class scala.concurrent.Future$.**",
  "-keep class scala.slick.driver.JdbcProfile$Implicits"
)

proguardCache in Android ++= Seq(
  //ProguardCache("slick") % "com.typesafe.slick"
)